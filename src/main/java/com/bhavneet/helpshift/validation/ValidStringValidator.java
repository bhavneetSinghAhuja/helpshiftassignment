package com.bhavneet.helpshift.validation;

import com.bhavneet.helpshift.exceptions.InvalidNameException;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class ValidStringValidator implements Validator{

    public void validate(String name) throws InvalidNameException {
        if(!StringUtils.isAlpha(name))
            throw new InvalidNameException("Invalid name:- "+name);
    }
}
