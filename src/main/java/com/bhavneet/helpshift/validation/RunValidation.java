package com.bhavneet.helpshift.validation;

import com.bhavneet.helpshift.exceptions.InvalidNameException;
import com.google.inject.Singleton;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
@Singleton
public class RunValidation {
    public void run(String name) throws InvalidNameException {
        //TODO use reflections to find out all classes implementing interface Validator and then run validate method on them one by one in a loop
        ValidStringValidator validStringValidator =new ValidStringValidator();
        validStringValidator.validate(name);
    }
}
