package com.bhavneet.helpshift.validation;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public interface Validator {
    public void validate(String name) throws Exception;
}
