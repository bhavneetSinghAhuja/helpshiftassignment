package com.bhavneet.helpshift.application;

import com.bhavneet.helpshift.core.Contacts;
import com.bhavneet.helpshift.di.HelpshiftModule;
import com.bhavneet.helpshift.exceptions.BadInputException;
import com.bhavneet.helpshift.validation.RunValidation;
import com.google.inject.Guice;
import com.google.inject.Injector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class Main {

    private static int getIntegerFromString(String numStr) {
        return Integer.parseInt(numStr);
    }

    private static String[] getSplitSpace(String line) {
        return line.split("\\s");
    }

    private static BufferedReader getBufferedReader() {
        return new BufferedReader(new InputStreamReader(System.in));
    }

    public static void main(String[] args) throws IOException {
        Injector injector= Guice.createInjector(new HelpshiftModule());
        Contacts contacts=injector.getInstance(Contacts.class);
        BufferedReader bi = getBufferedReader();
        String line;
        int n;
        while(true){
            System.out.println("1) Add contact 2) Search 3) Exit");
            line=bi.readLine();
            n=getIntegerFromString(line);
            if(n==1){
                line=bi.readLine();
                if(contacts.insertContact(line)){
                    System.out.println("Contact added successfully");
                }
                else{
                    System.out.println("Contact already exists");
                }
            }
            else if (n==2){
                line=bi.readLine();
                contacts.searchContact(line).forEach(System.out::println);
            }
            else if (n==3){
                System.out.println("Happy Searching");
                System.exit(0);//exit gracefully
            }
            else {
                throw new BadInputException("Wrong Input parameter:- "+n);
            }
        }
    }
}
