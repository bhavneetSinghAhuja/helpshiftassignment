package com.bhavneet.helpshift.di;

import com.bhavneet.helpshift.validation.RunValidation;
import com.google.inject.Provider;

/**
 * Created by bhavneet.ahuja on 30/11/16.
 */
public class HelpshiftProvider implements Provider<RunValidation> {

    @Override
    public RunValidation get() {
        return new RunValidation();
    }
}
