package com.bhavneet.helpshift.di;

import com.bhavneet.helpshift.validation.NameValidator;
import com.bhavneet.helpshift.validation.Validator;
import com.google.inject.AbstractModule;

/**
 * Created by bhavneet.ahuja on 29/11/16.
 */
public class HelpshiftModule extends AbstractModule {

    @Override
    protected void configure() {
        binder().bind(Validator.class).to(NameValidator.class);
    }
}
