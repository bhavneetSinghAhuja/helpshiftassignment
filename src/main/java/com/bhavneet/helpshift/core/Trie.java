package com.bhavneet.helpshift.core;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class Trie {
    private Node root;
    private Map<String, Set<String>> nameToFullName = new HashMap<>();
    Set<String> allNames=new HashSet<>();
    public Trie() {
        root = new Node();
    }

    private void insert(String name) {
        name = name.toLowerCase();
        Map<Character, Node> children = root.children;

        for(int i=0; i<name.length(); i++){
            char c = name.charAt(i);

            Node node;
            if (children.containsKey(c)) {
                node = children.get(c);
            } else {
                node = new Node(c);
                children.put(c, node);
            }
            children = node.children;

            if (i == name.length() - 1) { node.isTerminal = true; }
        }
    }

    public boolean insertFullName(String fullName) {
        if (allNames.contains(StringUtils.lowerCase(fullName))) return false;
        allNames.add(StringUtils.lowerCase(fullName));
            String fullNameLower = fullName.toLowerCase();
            String[] names = fullNameLower.split("\\s+");

            for (String name : names) {
                if (!nameToFullName.containsKey(name)) {
                    nameToFullName.put(name, new HashSet<>());
                }
                nameToFullName.get(name).add(fullName);
                insert(name);
            }
            return true;

    }

    private List<String> getAllStringWithPrefix(String prefix) {
        Node node = searchNode(prefix);
        if (node == null ) {
            return Collections.emptyList();
        }
        List<String> names = new ArrayList<>();
        getAllStringWithPrefix(node, prefix, names);
        return names;
    }

    public Set<String> getFullNamesWithPrefix(String prefix) {
        List<String> names = getAllStringWithPrefix(prefix.toLowerCase());
        Set<String> allNames =  names.stream().map(nameToFullName::get).flatMap(Collection::stream).collect(Collectors.toSet());
        if (allNames.contains(prefix)) {
            LinkedHashSet<String> relevanceSet = new LinkedHashSet<>();
            relevanceSet.add(prefix);
            relevanceSet.addAll(allNames);
            return relevanceSet;
        } else {
            return allNames;
        }
    }

    private void getAllStringWithPrefix(Node start, String current, List<String> names) {

        if (start.isTerminal) {
            names.add(current);
        }
        if (start.children.isEmpty()) {
            return;
        }
        // Get all strings that can be formed by the subtree of node.
        start.children.entrySet().forEach(e -> getAllStringWithPrefix(e.getValue(), current + e.getKey(), names));
    }

    private Node searchNode(String str){
        Map<Character, Node> children = root.children;
        Node t = null;
        for(int i=0; i<str.length(); i++){
            char c = str.charAt(i);
            if(children.containsKey(c)){
                t = children.get(c);
                children = t.children;
            }else{
                return null;
            }
        }
        return t;
    }
}

class Node {
    char c;
    Map<Character, Node> children = new HashMap<Character, Node>();
    boolean isTerminal;

    public Node() {}

    public Node(char c){
        this.c = c;
    }
}