package com.bhavneet.helpshift.core;

import com.bhavneet.helpshift.exceptions.InvalidNameException;
import com.bhavneet.helpshift.validation.RunValidation;
import com.google.inject.Inject;

import java.util.*;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */

public class Contacts {
    private Trie trie;

    @Inject
    public Contacts(RunValidation runValidation){
        trie=new Trie();
        this.runValidation=runValidation;
    }
    private final RunValidation runValidation;

    public boolean insertContact(String name) throws InvalidNameException {
        String names[]=name.split("\\s+");

        //validate name
        validateContact(names);

        return trie.insertFullName(name);
    }

    public Set<String> searchContact(String name) throws InvalidNameException {
        String names[]=name.split("\\s+");

        //validate name
        validateContact(names);
        return trie.getFullNamesWithPrefix(name);
    }

    private void validateContact(String[] names) throws InvalidNameException {
        for (int i = 0; i < names.length; i++) {
            runValidation.run(names[i]);
        }
    }

}
