package com.bhavneet.helpshift.exceptions;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class BadInputException extends IllegalArgumentException {
    public BadInputException(String message){super(message);}
}
