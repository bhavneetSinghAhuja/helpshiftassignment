package com.bhavneet.helpshift.exceptions;

import java.io.IOException;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class InvalidNameException extends IOException {
    public InvalidNameException(String message) {
        super(message);
    }
}