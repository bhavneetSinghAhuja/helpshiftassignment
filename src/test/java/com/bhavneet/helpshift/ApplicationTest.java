package com.bhavneet.helpshift;

import com.bhavneet.helpshift.core.Contacts;
import com.bhavneet.helpshift.di.HelpshiftModule;
import com.bhavneet.helpshift.exceptions.InvalidNameException;
import com.google.inject.Guice;
import com.google.inject.Injector;

import java.util.Set;

/**
 * Created by bhavneet.ahuja on 26/11/16.
 */
public class ApplicationTest {
    public static void main(String[] args) throws InvalidNameException {
        Injector injector= Guice.createInjector(new HelpshiftModule());
        Contacts contacts=injector.getInstance(Contacts.class);
        contacts.insertContact("Chris");
        contacts.insertContact("Chris");
        contacts.insertContact("Christ");
        contacts.insertContact("Christ Harry");
        contacts.insertContact("Ron Ron");
        contacts.insertContact("Ron");
        contacts.insertContact("Harry Potter");
        contacts.insertContact("Chris Harry Potter Ronald");
        contacts.insertContact("Ron Ronald");
        contacts.insertContact("Christine");
        contacts.insertContact("Ron Christine");
        testcase1(contacts);
        testcase2(contacts);
        testcase3(contacts);
        testcase4(contacts);
        testcase5(contacts);
        testcase6(contacts);
    }

    //relevance testcase
    private static boolean testcase6(Contacts contacts) throws InvalidNameException {
        Set<String> results=contacts.searchContact("Ron");
        if(results.iterator().next()=="Ron"){
            System.out.println("testcase6 passed");
            return true;
        }
        System.out.println("testcase6 failed");
        return false;
    }

    //successful insert testcase
    private static boolean testcase5(Contacts contacts) throws InvalidNameException {
        boolean flag=contacts.insertContact("Ron");
        if(flag) System.out.println("testcase5 failed");
        else System.out.println("testcase5 passed");
        return flag;
    }


    //Expects InvalidNameException to be thrown
    private static boolean testcase4(Contacts contacts) {
        try {
            contacts.insertContact("&%$");
            System.out.println("testcase4 failed");
            return false;
        } catch (InvalidNameException e) {
            e.printStackTrace();
            System.out.println("testcase4 passed");
            return true;
        }
    }

    //successful results
    private static boolean testcase3(Contacts contacts) throws InvalidNameException {

        Set<String> results=contacts.searchContact("Ron");
        if (results.isEmpty()){
            System.out.println("testcase3 failed");
            return false;
        }
        else{
            System.out.println("testcase3 passed");
            return true;
        }
    }

    //non inserted elements existence
    private static boolean testcase2(Contacts contacts) throws InvalidNameException {
        Set<String> results=contacts.searchContact("Bhavneet");
        if (results.isEmpty()){
            System.out.println("testcase2 failed");
            return false;
        }
        else{
            System.out.println("testcase2 passed");
            return true;
        }
    }

    private static boolean testcase1(Contacts contacts) throws InvalidNameException {
        boolean flag=contacts.insertContact("Bhavneet Singh Ahuja");
        if(!flag){
            System.out.println("testcase1 failed");
            return false;
        }
        Set<String> results=contacts.searchContact("Bhavneet");
        if (results.isEmpty()){
            System.out.println("testcase1 failed");
            return false;
        }
        else{
            System.out.println("testcase1 passed");
            return true;
        }
    }
}
